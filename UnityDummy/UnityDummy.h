//
//  UnityDummy.h
//  LibraryTest
//
//  Created by Piotr Jatrzębski on 24/07/16.
//  Copyright © 2016 Piotr Jatrzębski. All rights reserved.
//

#ifndef UnityDummy_h
#define UnityDummy_h

// Dummy.h/c is a simple alternative to "-Wl,-U,_UnitySendMessage" linker flag

#include <stdio.h>

void UnitySendMessage(const char * objectName, const char * messageName, const char * parameterString);


#endif /* UnityDummy_h */
